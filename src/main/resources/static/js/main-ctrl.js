'use strict';

angular.module('collage')
  .controller('MainCtrl', ['$scope', '$q', '$log', '$timeout', '$modal', 'ApiFactory', MainCtrl]);

function MainCtrl($scope, $q, $log, $timeout, $modal, ApiFactory) {

  var onError = function (error) {
    $log.error(error);

    if ($scope.modalVisible) {
      return;
    }

    if (error.status === 405) {
      error.data.error = 'Відмовлено в доступі';
      error.data.message = 'Обраний користувач не дозволяє Вам дивитися його фото';
      error.data.path = null;
    }

    $scope.error = error;

    var modalInstance = $modal.open({
      animation: true,
      backdrop: true,
      size: 'sm',
      templateUrl: 'adhoc/error-modal.html',
      controller: 'ErrorModalCtrl',
      scope: $scope
    });
    $scope.modalVisible = true;

    modalInstance.result.then(
      function () {
        $scope.modalVisible = false;
      }, function () {
        $scope.modalVisible = false;
      });
  };

  var onImagesLoaded = function (data) {
    $log.info('HTTP: Got items:', data.length);

    $scope.imageTable = [];
    var i = 0;
    for (var j = 0; j < $scope.params.rows; j++) {
      var arr = [];
      for (var k = 0; k < $scope.params.cols; k++) {
        if (i < data.length) {
          arr.push(data[i]);
        }
        i++;
      }
      $scope.imageTable.push(arr);
    }
    $scope.images = data;

    if (i <= data.length) {
      $scope.disableSubmit = false;
    } else {
      $scope.disableSubmit = true;
    }
  };

  var getMediaFeed = function () {
    ApiFactory.getMediaFeed(
      {
        count: $scope.params.cols * $scope.params.rows
      },
      onImagesLoaded,
      onError
    );
  };

  var getMediaUser = function () {
    ApiFactory.getMediaUser(
      {
        count: $scope.params.cols * $scope.params.rows,
        userId: $scope.userId
      },
      onImagesLoaded,
      onError
    );
  };

  var getUserList = function (search) {
    var deferred = $q.defer();

    ApiFactory.getUserList(
      {search: search},
      function (data) {
        deferred.resolve(data);
      },
      function (error) {
        deferred.reject();
        onError(error);
      }
    );

    return deferred.promise;
  };

  var onFormChange = function () {
    switch ($scope.feedToggle.current.id) {
      case 'mediaFeed':
        getMediaFeed();
        break;
      case 'mediaUser':
        getMediaUser();
        break;
    }
  };

  var timeout = null;

  var onFormChangeDelayed = function () {
    $scope.disableSubmit = true;

    if (timeout) {
      $timeout.cancel(timeout);
    }
    timeout = $timeout(function() {
      onFormChange();
    }, 300);
  };

  var onCollageComplete = function (data) {
    $log.info('HTTP: Got collage complete:', data.id, data.status);
    if (data.status === 'DONE') {

      $scope.collage = data;

      $modal.open({
        animation: true,
        backdrop: 'static',
        size: 'lg',
        templateUrl: 'adhoc/collage-modal.html',
        controller: 'CollageModalCtrl',
        scope: $scope
      });

    } else {
      onError({
        status: data.status,
        data: {
          error: 'Collage creation failed',
          message: data.status
        }
      })
    }
  };

  var getRatio = function() {
    return $scope.params.cols / $scope.params.rows;
  };

  var updateWidth = function () {
    $scope.params.width = Math.floor(getRatio() * $scope.params.height);
  };

  var updateHeight = function () {
    $scope.params.height = Math.floor($scope.params.width / getRatio());
  };

  $scope.images = [];
  $scope.imageTable = [];
  $scope.usersFound = {};
  $scope.modalVisible = false;
  $scope.disableSubmit = true;
  $scope.params = {
    cols: 2,
    rows: 2,
    width: 600,
    height: 600
  };

  $scope.$watch('params.cols', function () {
    updateWidth();
    onFormChangeDelayed();
  });
  $scope.$watch('params.rows', function () {
    updateHeight();
    onFormChangeDelayed();
  });
  $scope.$watch('params.width', updateHeight);
  $scope.$watch('params.height', updateWidth);

  $scope.$watch('userSearch', function () {
    $log.info('User selected:', $scope.userSearch);
    if ($scope.userSearch) {
      if ($scope.usersFound[$scope.userSearch]) {
        $scope.userId = $scope.usersFound[$scope.userSearch];
        $log.info('Current userId:', $scope.userId);
        if ($scope.feedToggle.current.id === 'mediaFeed') {
          $scope.feedToggle.doToggle();
        } else {
          onFormChange();
        }
      }
    }
  });

  $scope.feedToggle = {
    current: {
      id: 'mediaFeed',
      name: 'Моя стрічка'
    },
    alternate: {
      id: 'mediaUser',
      name: 'Фото користувача'
    },
    doToggle: function () {
      var tmp = $scope.feedToggle.current;
      $scope.feedToggle.current = $scope.feedToggle.alternate;
      $scope.feedToggle.alternate = tmp;
      onFormChange();
    }
  };

  $scope.onCollage = function () {
    var request = {
      cols: $scope.params.cols,
      rows: $scope.params.rows,
      width: $scope.params.width,
      height: $scope.params.height,
      items: $scope.images
    };

    ApiFactory.getCollage(
      request,
      onCollageComplete,
      onError
    );
  };

  $scope.searchUsers = function (search) {
    return getUserList(search)
      .then(function (data){
        $scope.usersFound = {};
        var users = [];
        for (var i = 0; i < data.length; i++) {
          users.push(data[i].name);
          $scope.usersFound[data[i].name] = data[i].id;
        }
        return users;
      });
  };

  //onFormChange();
}
