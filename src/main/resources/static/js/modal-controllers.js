'use strict';

angular.module('collage')
  .controller('ErrorModalCtrl', ['$scope', '$modalInstance', ErrorModalCtrl]);

function ErrorModalCtrl($scope, $modalInstance) {

  $scope.onCancel = function () {
    $modalInstance.dismiss('cancel');
  };

}


angular.module('collage')
  .controller('CollageModalCtrl', ['$scope', '$modalInstance', CollageModalCtrl]);

function CollageModalCtrl($scope, $modalInstance) {

  $scope.onCancel = function () {
    $modalInstance.dismiss('cancel');
  };

}
