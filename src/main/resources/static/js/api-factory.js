'use strict';

angular.module('collage')
  .factory('ApiFactory', ['$resource', ApiFactory]);

function ApiFactory($resource) {
  return $resource('/api',
    {id: '@id'}, {
      getMediaFeed: {method: 'GET', isArray: true, params: {count: 10}, url: '/api/media/feed'},
      getMediaUser: {method: 'GET', isArray: true, params: {count: 10}, url: '/api/media/recent'},
      getCollage: {method: 'POST', isArray: false, url: '/api/collage'},
      getUserList: {method: 'GET', isArray: true, url: '/api/users/search'}
    });
}
