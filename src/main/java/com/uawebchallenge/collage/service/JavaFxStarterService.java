package com.uawebchallenge.collage.service;

import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

/**
 * Helper service used to enable JavaFX components
 * <p/>
 * Unfortunately, JavaFX requires not only to be initialized,<br>
 * but also the whole application cannot run on the environment without active window system.<br>
 * This resulted in impossibility to deploy application to most of Cloud services and put the developer in a deepest frustration.<br>
 * That's why, all JavaFX-related code should be wiped out from entire project, and its name to be forgotten.
 */
@Service
public class JavaFxStarterService implements InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(JavaFxStarterService.class);

    /**
     * Wait till JavaFX fully starts
     */
    private static CountDownLatch latch = new CountDownLatch(1);

    /**
     * Prevent multiple starts
     */
    private static boolean started = false;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.debug("Initializing JavaFX");

        if (started) {
            log.debug("Already initialized");
            return;
        }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                new JavaFxApplication().launchApplication();
            }
        };

        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.start();

        latch.await();
        started = true;
    }

    public static class JavaFxApplication extends Application {

        @Override
        public void start(Stage primaryStage) throws Exception {
            latch.countDown();
        }

        public void launchApplication() {
            log.debug("Launching JavaFX application");
            launch();
        }
    }

}
