package com.uawebchallenge.collage.service;

import com.uawebchallenge.collage.dto.MediaDto;
import com.uawebchallenge.collage.dto.UserInfoDto;
import org.jinstagram.Instagram;
import org.jinstagram.auth.InstagramAuthService;
import org.jinstagram.auth.model.Token;
import org.jinstagram.auth.model.Verifier;
import org.jinstagram.auth.oauth.InstagramService;
import org.jinstagram.entity.users.basicinfo.UserInfoData;
import org.jinstagram.entity.users.feed.MediaFeed;
import org.jinstagram.entity.users.feed.MediaFeedData;
import org.jinstagram.entity.users.feed.UserFeed;
import org.jinstagram.entity.users.feed.UserFeedData;
import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides access to the Instagram API.
 * <p/>
 * Prior to use this service, access-token must be retrieved, as result of Oauth2 authorization.<br>
 * This service uses jInstagram InstagramService for API calls.
 * <p/>
 * Service uses important configuration from application.properties / application-{profile}.properties files:<br>
 * instagram.api.key for API access<br>
 * instagram.api.secret for API access<br>
 * instagram.callback.url for proper Oauth2 redirect<br>
 *
 * @see org.jinstagram.auth.oauth.InstagramService
 */
@Service
public class InstagramApiService implements InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(InstagramApiService.class);

    /**
     * As recommended by jInstagram
     */
    private static final Token EMPTY_TOKEN = null;

    /**
     * The guy who does all the job
     */
    private InstagramService service;

    @Autowired
    private ValidationService validationService;

    @Value("${instagram.api.key}")
    private String apiKey;

    @Value("${instagram.api.secret}")
    private String apiSecret;

    @Value("${instagram.callback.url}")
    private String callbackUrl;

    /**
     * Obtains Instagram Oauth2 authorization URL
     */
    public String getAuthUrl() {
        return service.getAuthorizationUrl(EMPTY_TOKEN);
    }

    /**
     * Retrieves Access token using Instagram Oauth2 auth code.
     *
     * @param code Oauth2 successful response code, must be not null.
     * @return New access-token.
     */
    public Token getAccessTokenByOauthResponseCode(String code) {
        log.debug("Getting access token by code: {}", code);
        validationService.validateNotEmpty(code);

        Verifier verifier = new Verifier(code);
        return service.getAccessToken(EMPTY_TOKEN, verifier);
    }

    /**
     * Fetches recent items from current User's feed.<br>
     * Only basic media info is retrieved, see MediaDto.<br>
     * User is determined by access-token.
     *
     * @param token Current access-token.
     * @param count Desired items count. Service may return less items  (if feed doesn't contain required quantity),
     *              but no more than requested.
     * @return list of simplified DTOs.
     * @throws InstagramException If request to Instagram fails.
     * @see com.uawebchallenge.collage.dto.MediaDto.
     */
    public List<MediaDto> getMediaFeed(Token token, Integer count) throws InstagramException {
        log.debug("Getting {} MediaFeed items", count);
        validationService.validateNotNull(token);
        validationService.validateGreaterThanZero(count);

        Instagram instagram = new Instagram(token);
        MediaFeed feed = instagram.getUserFeeds(null, null, count);

        List<MediaFeedData> feedDataList = completeExactPagination(instagram, feed, count);
        return getMediaDtoList(feedDataList);
    }

    /**
     * Fetches recent items owned by specific User, identified by id.<br>
     * Only basic media info is retrieved, see MediaDto.<br>
     *
     * @param token  Current access-token.
     * @param userId Instagram User id. Can be retrieved with getCurrentUserInfo() or searchUsers().
     * @param count  Desired items count. Service may return less items (if feed doesn't contain required quantity),
     *               but no more than requested.
     * @return list of simplified DTOs.
     * @throws InstagramException If request to Instagram fails.
     * @see com.uawebchallenge.collage.dto.MediaDto
     */
    public List<MediaDto> getMediaUser(Token token, String userId, Integer count) throws InstagramException {
        log.debug("Getting {} MediaUser items", count);
        validationService.validateNotNull(token);
        validationService.validateGreaterThanZero(count);

        Instagram instagram = new Instagram(token);
        MediaFeed feed = instagram.getRecentMediaFeed(userId, count, null, null, null, null);

        List<MediaFeedData> feedDataList = completeExactPagination(instagram, feed, count);
        return getMediaDtoList(feedDataList);
    }

    /**
     * Fetches basic User info by username search query.<br>
     *
     * @param token  Current access-token.
     * @param search Username search query string. Only Instagram is responsible for matching algorithm.
     * @param count  Desired items count. Service may return more or less items than requested.
     * @return List of basic Instagram User information.
     * @throws InstagramException If request to Instagram fails.
     * @see com.uawebchallenge.collage.dto.UserInfoDto
     */
    public List<UserInfoDto> searchUsers(Token token, String search, int count) throws InstagramException {
        log.debug("Getting users by search query: {}", search);
        validationService.validateNotNull(token);
        validationService.validateNotEmpty(search);
        validationService.validateGreaterThanZero(count);

        Instagram instagram = new Instagram(token);
        UserFeed feed = instagram.searchUser(search, count);

        List<UserFeedData> feedDataList = new ArrayList<>();
        feedDataList.addAll(feed.getUserList());

        while (feedDataList.size() < count && feed.getPagination() != null && feed.getPagination().hasNextPage()) {
            feed = instagram.getUserFeedInfoNextPage(feed.getPagination());
            feedDataList.addAll(feed.getUserList());
        }

        List<UserInfoDto> users = new ArrayList<>();
        for (UserFeedData data : feedDataList) {
            users.add(new UserInfoDto(data.getId(), data.getUserName()));
        }

        return users;
    }

    /**
     * Fetches complete current User's data.<br>
     * User is determined by access-token.
     *
     * @param token Current access-token.
     * @return Complete Instagram User data.
     * @throws InstagramException If request to Instagram fails.
     */
    public UserInfoData getCurrentUserInfo(Token token) throws InstagramException {
        log.debug("Getting current user id");
        validationService.validateNotNull(token);

        Instagram instagram = new Instagram(token);
        return instagram.getCurrentUserInfo().getData();
    }

    /**
     * Iterates through Instagram API pagination, till reaches requested item count.<br>
     * If summary with last request there are more then required items fetched, the list is trimmed.
     */
    private List<MediaFeedData> completeExactPagination(Instagram instagram, MediaFeed feed, Integer count) throws InstagramException {
        List<MediaFeedData> feedDataList = new ArrayList<>();
        feedDataList.addAll(feed.getData());

        while (feedDataList.size() < count && feed.getPagination() != null && feed.getPagination().hasNextPage()) {
            feed = instagram.getRecentMediaNextPage(feed.getPagination());
            feedDataList.addAll(feed.getData());
        }

        if (feedDataList.size() > count) {
            feedDataList = new ArrayList<>(feedDataList.subList(0, count));
        }

        return feedDataList;
    }

    /**
     * Builds simple MediaDto list from verbose MediaFeedData list
     */
    private List<MediaDto> getMediaDtoList(List<MediaFeedData> feedDataList) {
        List<MediaDto> mediaDtoList = new ArrayList<>();
        for (MediaFeedData data : feedDataList) {
            mediaDtoList.add(new MediaDto(
                    data.getId(),
                    data.getImages().getLowResolution().getImageUrl(),
                    data.getImages().getStandardResolution().getImageUrl()
            ));
        }
        return mediaDtoList;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        service = new InstagramAuthService()
                .apiKey(apiKey)
                .apiSecret(apiSecret)
                .callback(callbackUrl)
                .build();
    }

    void setService(InstagramService service) {
        this.service = service;
    }
}