package com.uawebchallenge.collage.service;

import com.uawebchallenge.collage.security.UserInfo;
import com.uawebchallenge.collage.security.GrantedAuthoritiesProvider;
import org.jinstagram.auth.model.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

/**
 * Handles User authentication via Instagram Oauth2.
 * <p/>
 * Both populates Spring SecurityContext with valid Authentication, and stores additional UserInfo<br>
 * in current HTTP session.<br>
 * If session becomes expired, all this data is removed.
 */
@Service
public class AuthService {

    private static final Logger log = LoggerFactory.getLogger(AuthService.class);

    /**
     * Key for Session value
     */
    public static final String USER_INFO = "oauth_user_info";

    @Autowired
    private ValidationService validationService;

    /**
     * Authenticates session with Oauth2 access token.
     * Populates Security Context with UsernamePasswordAuthentication based on token,
     * and also saves token as session attribute, among with additional UserInfo.<br>
     * All parameters are required.
     *
     * @param request  current HTTP request
     * @param userId   authenticated User's id
     * @param username authenticated User's username
     * @param token    Oauth2 access token
     */
    public void authenticate(WebRequest request, String userId, String username, Token token) {
        validationService.validateNotNull(request);
        validationService.validateNotEmpty(userId);
        validationService.validateNotEmpty(username);
        validationService.validateNotNull(token);

        UserInfo userInfo = new UserInfo(userId, username, token);
        request.setAttribute(USER_INFO, userInfo, RequestAttributes.SCOPE_SESSION);

        Authentication auth = new UsernamePasswordAuthenticationToken(
                userInfo.getId(), userInfo.getToken().getToken(), GrantedAuthoritiesProvider.getDefault());

        SecurityContextHolder.getContext().setAuthentication(auth);

        log.debug("Authenticated session with user id: {}", userId);
    }

    /**
     * Retrieve currently authenticated User's UserInfo.<br>
     * If UserInfo is not found in session, or user is not authenticated,<br>
     * all session's auth info is cleared and null is returned.
     *
     * @param request current HTTP request
     * @return UserInfo, or null if not authenticated
     */
    public UserInfo getUserInfoOrNull(WebRequest request) {
        validationService.validateNotNull(request);

        UserInfo userInfo = (UserInfo) request.getAttribute(USER_INFO, RequestAttributes.SCOPE_SESSION);
        log.debug("Getting user info: {}", userInfo);

        if (userInfo == null || !isAuthenticated()) {
            deAuthenticate(request);
            return null;
        }
        return userInfo;
    }

    /**
     * Same as getUserInfoOrNull, but throws AuthenticationException in case of null UserInfo.
     *
     * @throws org.springframework.security.authentication.InsufficientAuthenticationException
     * @see com.uawebchallenge.collage.service.AuthService#getUserInfoOrNull(org.springframework.web.context.request.WebRequest)
     */
    public UserInfo getUserInfo(WebRequest request) {
        UserInfo userInfo = getUserInfoOrNull(request);
        if (userInfo == null) {
            throw new InsufficientAuthenticationException("Session expired");
        }
        return userInfo;
    }

    /**
     * Remove authentication from current session
     * @param request current HTTP request
     */
    public void deAuthenticate(WebRequest request) {
        validationService.validateNotNull(request);

        request.removeAttribute(USER_INFO, RequestAttributes.SCOPE_SESSION);
        SecurityContextHolder.getContext().setAuthentication(null);
        log.info("User is now de-authenticated");
    }

    /**
     * @return true if Security Context contains valid authentication, else false
     */
    private boolean isAuthenticated() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null && auth.isAuthenticated();
    }

    /**
     * For Unit testing
     */
    void setValidationService(ValidationService validationService) {
        this.validationService = validationService;
    }
}