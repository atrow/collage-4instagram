package com.uawebchallenge.collage.service;

import com.uawebchallenge.collage.exception.BadRequestException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;


/**
 * One place for all validations
 */
@Service
public class ValidationService {

    /**
     * Fails if specified object is null
     */
    public void validateNotNull(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("Precondition failed: object is null");
        }
    }

    /**
     * Fails if specified string is null or empty
     */
    public void validateNotEmpty(String string) {
        validateNotNull(string);
        if (!StringUtils.hasText(string)) {
            throw new IllegalArgumentException(
                    String.format("Precondition failed: string is empty: %s", string)
            );
        }
    }

    /**
     * Fails if specified string is not a valid URL
     */
    public void validateIsUrl(String url) {
        try {
            new URL(url);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(
                    String.format("Precondition failed: invalid URL: %s", url), e
            );
        }
    }

    /**
     * Fails if specified integer is null, equal to zero, or less than zero
     */
    public void validateGreaterThanZero(Integer number) {
        validateNotNull(number);
        if (number <= 0) {
            throw new IllegalArgumentException(
                    String.format("Precondition failed: number: %s is less than or equal to zero", number)
            );
        }
    }


    public void validateCollectionSize(Collection<?> collection, int count) {
        validateNotNull(collection);
        validateNotNull(count);
        if (collection.size() != count) {
            throw new BadRequestException(
                    String.format("Precondition failed: collection size: %s doesn't match required size: %s", collection.size(), count)
            );
        }
    }

    public void validateRequestLimit(Integer count, int limit) {
        validateNotNull(count);
        if (count > limit) {
            throw new BadRequestException(
                    String.format("Request failed: requested count: %s exceeds the limit of: %s", count, limit)
            );
        }
    }
}