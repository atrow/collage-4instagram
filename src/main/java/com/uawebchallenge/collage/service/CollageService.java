package com.uawebchallenge.collage.service;

import com.uawebchallenge.collage.dto.CollageBuildingTaskDto;
import com.uawebchallenge.collage.dto.MediaDto;
import com.uawebchallenge.collage.exception.NotFoundException;
import com.uawebchallenge.collage.image.CollageBuildingTask;
import com.uawebchallenge.collage.image.TileResolveTask;
import com.uawebchallenge.collage.image.template.BasicGridCollage;
import com.uawebchallenge.collage.image.template.CollageTemplate;
import com.uawebchallenge.collage.image.template.TileTemplate;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorCompletionService;

/**
 * Performs Collage creation tasks
 * <p/>
 * On request, runs Collage creation task, using BasicGridCollage.<br>
 * Each tile for collage is being retrieved by TileResolveTask from web concurrently with a ThreadPool.<br>
 * <i>These tasks may also use some caching in future implementations</i>
 * <p/>
 * While Tiles are processed in parallel, entire collage is built synchronously.
 * <p/>
 * After finishing Collage building, service persists image data in the in-memory cache,<br>
 * and returns CollageBuildingTaskDto, containing task id.<br>
 * Client then may retrieve image by this id.
 * <p/>
 * Currently, items in cache are kept for TASK_VALIDITY_SECONDS, and then disposed, when next Collage creation tasks run.<br>
 * If user wants to get Collage after the task disposal, he must run a task again.
 *
 * @see com.uawebchallenge.collage.image.template.BasicGridCollage
 * @see com.uawebchallenge.collage.image.CollageBuildingTask
 * @see com.uawebchallenge.collage.config.TaskExecutorConfig
 */
@Service
public class CollageService {

    private static final Logger log = LoggerFactory.getLogger(CollageService.class);

    /**
     * Collage creation tasks will be disposed after
     */
    public static final int TASK_VALIDITY_SECONDS = 300;

    /**
     * Acts as in-memory cache
     */
    private ConcurrentHashMap<String, CollageBuildingTask> taskMap = new ConcurrentHashMap<>();

    @Autowired
    private ValidationService validationService;

    @Autowired
    @Qualifier("imageFetching")
    private TaskExecutor imageFetchingExecutor;

    /**
     * Run new Collage creation task<br>
     * BasicGridCollage will be created synchronously.
     *
     * @param cols   Number of collage columns.
     * @param rows   Number of collage columns.
     * @param width  Collage width in pixels.
     * @param height Collage height in pixels.
     * @param items  List of media items. Items count must be = cols * rows, or BadRequestException will be thrown.
     * @return CollageBuildingTaskDto, containing task id, in DONE status
     * @throws com.uawebchallenge.collage.exception.BadRequestException If arguments are invalid.
     */
    public CollageBuildingTaskDto runCollageCreation(Integer cols, Integer rows, final Integer width, final Integer height, final List<MediaDto> items) {
        validationService.validateGreaterThanZero(cols);
        validationService.validateGreaterThanZero(rows);
        validationService.validateGreaterThanZero(width);
        validationService.validateGreaterThanZero(height);
        validationService.validateCollectionSize(items, rows * cols);

        log.debug("Creating new collage of {} cols, {} rows, {} width, {} height, {} images", cols, rows, width, height, items.size());

        removeExpiredTasks();

        CollageBuildingTask collageTask = new CollageBuildingTask();
        taskMap.put(collageTask.getId(), collageTask);

        log.debug("new Collage task id: {}", collageTask.getId());

        try {
            CollageTemplate collageTemplate = new BasicGridCollage(cols, rows, width, height, items);

            ExecutorCompletionService<TileTemplate> completionService = new ExecutorCompletionService<>(imageFetchingExecutor);

            for (TileTemplate tileTemplate : collageTemplate.getTiles()) {
                TileResolveTask task = new TileResolveTask(tileTemplate);
                completionService.submit(task);
            }
            log.debug("all TileResolveTasks submitted");

            for (int i = 0; i < collageTemplate.getTiles().size(); i++) {
                TileTemplate tile = completionService.take().get();
                // Tile may not resolve, ignore it
                if (tile.isResolved()) {
                    collageTemplate.onTileResolved(tile);
                } else {
                    log.warn("Tile wasn't resolved: {}", tile);
                }
            }

            byte[] collage = collageTemplate.getBinaryImage();
            collageTask.setStatus(CollageBuildingTask.Status.DONE);
            collageTask.setImage(collage);

            log.debug("Collage task id: {} complete", collageTask.getId());

        } catch (Exception e) {
            collageTask.setStatus(CollageBuildingTask.Status.FAILED);
            throw new RuntimeException("Collage building task failed:", e);
        }

        return CollageBuildingTaskDto.fromTask(collageTask);
    }

    /**
     * Retrieve binary image data by Collage creation task id.<br>
     *
     * @param taskId Collage creation task id
     * @return Image byte array. Image format is JPEG.
     * @throws com.uawebchallenge.collage.exception.NotFoundException if Collage creation task is failed or expired
     */
    public byte[] getCollage(String taskId) {
        validationService.validateNotEmpty(taskId);
        log.debug("Getting Collage by task id: {}", taskId);

        CollageBuildingTask task = taskMap.get(taskId);
        if (task == null || task.getImage() == null) {
            throw new NotFoundException("Collage doesn't exist");
        }
        return task.getImage();
    }

    /**
     * Removes expired tasks from cache.
     */
    private void removeExpiredTasks() {
        log.debug("Removing expired Collage tasks");
        for (String id : taskMap.keySet()) {
            DateTime started = new DateTime(taskMap.get(id).getStarted());
            if (started.plusSeconds(TASK_VALIDITY_SECONDS).isBeforeNow()) {
                taskMap.remove(id);
                log.debug("Removed task: {}", id);
            }
        }
    }

}
