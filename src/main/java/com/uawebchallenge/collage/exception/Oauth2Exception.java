package com.uawebchallenge.collage.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Signals that User's Oauth2 authentication attempt failed.<br>
 * Status code: 401
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class Oauth2Exception extends RuntimeException {

    public Oauth2Exception(String message) {
        super(message);
    }

}
