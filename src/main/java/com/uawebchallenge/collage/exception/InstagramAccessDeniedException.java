package com.uawebchallenge.collage.exception;

import org.jinstagram.exceptions.InstagramBadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Signals that Instagram API denied an action for current authenticated User<br>
 * Status code: 405
 */
@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class InstagramAccessDeniedException extends RuntimeException {

    public InstagramAccessDeniedException(InstagramBadRequestException e) {
        super(e);
    }

}
