package com.uawebchallenge.collage.dto;

import com.uawebchallenge.collage.image.CollageBuildingTask;

/**
 * DTO represents Collage building task id and status.<br>
 * Used in Server -> Client response
 */
public class CollageBuildingTaskDto {

    /**
     * Task id
     */
    private String id;

    /**
     * Task status
     */
    private CollageBuildingTask.Status status;

    /**
     * Default constructor
     */
    public CollageBuildingTaskDto() {
    }

    /**
     * @param id     Collage building task id, may be not null
     * @param status Collage building status, may be not null
     */
    public CollageBuildingTaskDto(String id, CollageBuildingTask.Status status) {
        this.id = id;
        this.status = status;
    }

    /**
     * Simple DTO creation from Model object.<br>
     * Omits binary image data of CollageBuildingTask
     *
     * @param task Task to create DTO from.
     *             if task == null, the dummy DTO with EXPIRED status will be created.
     * @return DTO, filled with Task data, or dummy with EXPIRED status.
     */
    public static CollageBuildingTaskDto fromTask(CollageBuildingTask task) {
        if (task == null) {
            return new CollageBuildingTaskDto(null, CollageBuildingTask.Status.EXPIRED);
        }
        return new CollageBuildingTaskDto(task.getId(), task.getStatus());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CollageBuildingTask.Status getStatus() {
        return status;
    }

    public void setStatus(CollageBuildingTask.Status status) {
        this.status = status;
    }
}
