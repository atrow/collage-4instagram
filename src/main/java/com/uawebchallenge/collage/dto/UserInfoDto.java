package com.uawebchallenge.collage.dto;

/**
 * Represents basic info about Instagram User.<br>
 * Used as response to Client's Search user request.
 */
public class UserInfoDto {

    /**
     * Instagram User Id, numeric string
     */
    private String id;

    /**
     * Instagram User username
     */
    private String name;

    public UserInfoDto() {
    }

    public UserInfoDto(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
