package com.uawebchallenge.collage.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * DTO represents Collage building request received from client.<br>
 * Used in Client -> Server request
 */
public class CreateCollageRequestDto {

    /**
     * Requested columns number.<br>
     * Cannot be null, less than 1, more than 32
     */
    @NotNull
    @Min(1)
    @Max(32)
    private Integer cols;

    /**
     * Requested rows number.<br>
     * Cannot be null, less than 1, more than 32
     */
    @NotNull
    @Min(1)
    @Max(32)
    private Integer rows;

    /**
     * Requested collage width in pixels.<br>
     * Cannot be null, less than 10, more than 3200
     */
    @NotNull
    @Min(10)
    @Max(3200)
    private Integer width;

    /**
     * Requested collage height in pixels.<br>
     * Cannot be null, less than 10, more than 3200
     */
    @NotNull
    @Min(10)
    @Max(3200)
    private Integer height;

    /**
     * Requested collage Media items<br>
     * Cannot be null or empty.
     */
    @NotEmpty
    private List<MediaDto> items;


    public Integer getCols() {
        return cols;
    }

    public void setCols(Integer cols) {
        this.cols = cols;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public List<MediaDto> getItems() {
        return items;
    }

    public void setItems(List<MediaDto> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "CreateCollageRequestDto{" +
                "cols=" + cols +
                ", rows=" + rows +
                ", width=" + width +
                ", height=" + height +
                ", items=" + items +
                '}';
    }
}
