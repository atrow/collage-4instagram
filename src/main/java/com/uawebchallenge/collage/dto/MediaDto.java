package com.uawebchallenge.collage.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Represents single Media item from Instagram feed.<br>
 * Used both in Server -> Client response and Client -> Server request
 */
public class MediaDto {

    /**
     * Media item Id.<br>
     * Cannot be null or empty
     */
    @NotEmpty
    private String id;

    /**
     * Media item Low resolution URL.<br>
     * Cannot be null or empty
     */
    @NotEmpty
    private String lowResUrl;

    /**
     * Media item High resolution URL.<br>
     * Cannot be null or empty
     */
    @NotEmpty
    private String highResUrl;

    public MediaDto() {
    }

    public MediaDto(String id, String lowResUrl, String highResUrl) {
        this.id = id;
        this.lowResUrl = lowResUrl;
        this.highResUrl = highResUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLowResUrl() {
        return lowResUrl;
    }

    public void setLowResUrl(String lowResUrl) {
        this.lowResUrl = lowResUrl;
    }

    public String getHighResUrl() {
        return highResUrl;
    }

    public void setHighResUrl(String highResUrl) {
        this.highResUrl = highResUrl;
    }

    @Override
    public String toString() {
        return "MediaDto{" +
                "id='" + id + '\'' +
                ", lowResUrl='" + lowResUrl + '\'' +
                ", highResUrl='" + highResUrl + '\'' +
                '}';
    }
}
