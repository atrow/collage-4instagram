package com.uawebchallenge.collage.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;

/**
 * Provides basic GrantedAuthorities for authenticated users
 * @see org.springframework.security.core.GrantedAuthority
 */
public class GrantedAuthoritiesProvider {

    /**
     * Default role name, common for all authenticated users
     */
    public static final String USER = "ROLE_USER";

    /**
     * Disable instantiation
     */
    private GrantedAuthoritiesProvider() {
        // Empty
    }

    /**
     * @return Single GrantedAuthority with role ROLE_USER
     */
    public static List<? extends GrantedAuthority> getDefault() {
        return Collections.singletonList(
                new SimpleGrantedAuthority(USER)
        );
    }

}
