package com.uawebchallenge.collage.security;

import org.jinstagram.auth.model.Token;

/**
 * Represents an authenticated service User.<br>
 * Contains necessary Instagram user information: access-token, ID, username.
 */
public class UserInfo {

    /**
     * Instagram user id
     */
    private final String id;

    /**
     * Instagram username
     */
    private final String name;

    /**
     * Instagram API access-token
     */
    private final Token token;

    /**
     * Create UserInfo as result of successful authentication.
     *
     * @param id    User Id retrieved from Instagram API.
     * @param name  User username retrieved from Instagram API.
     * @param token Access-token retrieved as a result of Oauth2 authorisation.
     */
    public UserInfo(String id, String name, Token token) {
        this.id = id;
        this.name = name;
        this.token = token;
    }

    /**
     * @return Instagram user id
     */
    public String getId() {
        return id;
    }

    /**
     * @return Instagram username
     */
    public String getName() {
        return name;
    }

    /**
     * @return Instagram API access-token
     */
    public Token getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", token=" + token +
                '}';
    }
}
