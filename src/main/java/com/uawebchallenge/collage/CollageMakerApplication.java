package com.uawebchallenge.collage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;

/**
 * Main class for Spring Boot application.<br>
 * Capable for running from command line using embedded Tomcat.
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = {
        SecurityAutoConfiguration.class
})
public class CollageMakerApplication {

    /**
     * Command line execution entry point
     */
    public static void main(String[] args) {
        SpringApplication.run(CollageMakerApplication.class, args);
    }

}
