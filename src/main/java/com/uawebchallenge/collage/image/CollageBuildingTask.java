package com.uawebchallenge.collage.image;

import java.util.Date;
import java.util.UUID;

/**
 * Represents a status of a whole single Collage building process.
 * <p/>
 * When task is finished, it should contain completion status,<br>
 * and in case of success, resulting binary image.
 * <p/>
 * Use <i>getStarted()</i> and <i>getStatus()</i> to properly dispose tasks.
 */
public class CollageBuildingTask {

    private final String id;

    private final Date started;

    private Status status;

    private byte[] image;

    /**
     * Represents task status
     */
    public static enum Status {
        /**
         * Task was started
         */
        RUNNING,
        /**
         * Task execution failed
         */
        FAILED,
        /**
         * Task successfully complete. Collage is ready
         */
        DONE,
        /**
         * Task can't be found, possibly it was disposed
         */
        EXPIRED
    }

    /**
     * Create new task in RUNNING state with started date set to now.
     */
    public CollageBuildingTask() {
        id = UUID.randomUUID().toString();
        started = new Date();
        status = Status.RUNNING;
    }

    /**
     * @return Unique Task id, UUID
     */
    public String getId() {
        return id;
    }

    /**
     * @return Task start date
     */
    public Date getStarted() {
        return started;
    }

    /**
     * @return Task status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status Update task status
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return Fetch image from task. null if status is not DONE
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * @param image Update task image
     */
    public void setImage(byte[] image) {
        this.image = image;
    }
}
