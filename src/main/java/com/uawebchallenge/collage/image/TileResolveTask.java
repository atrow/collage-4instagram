package com.uawebchallenge.collage.image;

import com.uawebchallenge.collage.image.template.TileTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * Callable wrapper for TileTemplate resolving.<br>
 * Allows to run Tile resolving with a controllable parallelism.
 */
public class TileResolveTask implements Callable<TileTemplate> {

    private static final Logger log = LoggerFactory.getLogger(TileResolveTask.class);

    /**
     * Single Tile to resolve
     */
    private final TileTemplate tile;

    /**
     * Creates a Callable task for single tile resolving.
     *
     * @param tile Tile to resolve
     * @see com.uawebchallenge.collage.image.template.CollageTemplate
     */
    public TileResolveTask(TileTemplate tile) {
        this.tile = tile;
    }

    /**
     * Resolves a tile, or throws an exception if unable to do so.
     * @return Tile in resolved state, with not-null Image
     * @throws Exception if something goes wrong
     */
    @Override
    public TileTemplate call() throws Exception {
        log.debug("Going to resolve tile: {}", tile.getUrl());
        tile.resolve();
        return tile;
    }

}
