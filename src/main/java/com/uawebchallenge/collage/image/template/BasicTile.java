package com.uawebchallenge.collage.image.template;

import com.uawebchallenge.collage.image.Offset;
import javafx.scene.image.Image;

/**
 * Basic tile which resolve its Image to a specific size.<br>
 * Resulting size may differ from requested.<br>
 * Aspect ratio is preserved.
 */
public class BasicTile implements TileTemplate {

    /**
     * Requested image width
     */
    private final Integer width;

    /**
     * Requested image height
     */
    private final Integer height;

    /**
     * Tile offset in a collage
     */
    private final Offset offset;

    /**
     * URL to resolve image from
     */
    private final String url;

    /**
     * Resolved image
     */
    private Image image;

    /**
     * Resulting tile's image size may differ from requested.
     * @param width Requested image width
     * @param height Requested image height
     * @param offset Tile offset in a collage
     * @param url URL to resolve image from
     */
    public BasicTile(Integer width, Integer height, Offset offset, String url) {
        this.width = width;
        this.height = height;
        this.offset = offset;
        this.url = url;
    }

    @Override
    public Integer getWidth() {
        return width;
    }

    @Override
    public Integer getHeight() {
        return height;
    }

    @Override
    public Offset getOffset() {
        return offset;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public boolean isResolved() {
        return image != null;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This implementation uses JavaFX Image
     */
    @Override
    public void resolve() {
        this.image = new Image(url, width, height, true, true);
    }

    @Override
    public String toString() {
        return "BasicTile{" +
                "width=" + width +
                ", height=" + height +
                ", offset=" + offset +
                ", url='" + url + '\'' +
                '}';
    }
}
