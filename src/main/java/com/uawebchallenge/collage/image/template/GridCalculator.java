package com.uawebchallenge.collage.image.template;

import com.uawebchallenge.collage.image.Offset;

/**
 * Helper class to calculate tile sizes and positions inside a basic grid collage
 */
public class GridCalculator {

    private final Integer cols;

    private final Integer rows;

    private final Integer width;

    private final Integer height;

    /**
     * @param cols   Collage columns
     * @param rows   Collage rows
     * @param width  Collage width in pixels
     * @param height Collage height in pixels
     */
    public GridCalculator(Integer cols, Integer rows, Integer width, Integer height) {
        this.cols = cols;
        this.rows = rows;
        this.width = width;
        this.height = height;
    }

    /**
     * @return single tile width
     */
    public Double getTileWidth() {
        return width.doubleValue() / cols;
    }

    /**
     * @return single tile height
     */
    public Double getTileHeight() {
        return height.doubleValue() / rows;
    }

    /**
     * @return Tile offset basing on its position in a collage
     */
    public Offset getTileOffset(int col, int row) {
        int x = col * width / cols;
        int y = row * height / rows;
        return new Offset(x, y);
    }
}
