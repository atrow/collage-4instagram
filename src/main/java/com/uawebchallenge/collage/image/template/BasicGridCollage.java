package com.uawebchallenge.collage.image.template;

import com.uawebchallenge.collage.dto.MediaDto;
import com.uawebchallenge.collage.image.ImageBuilder;
import com.uawebchallenge.collage.image.Offset;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Simple collage, which puts tiles into fixed grid.
 * <p/>
 * For additional info, see CollageTemplate.
 *
 * @see com.uawebchallenge.collage.image.template.CollageTemplate
 */
public class BasicGridCollage implements CollageTemplate {

    /**
     * Used to create and encode image
     */
    private final ImageBuilder imageBuilder;

    /**
     * Tiles for collage building
     */
    private final List<TileTemplate> tiles = new ArrayList<>();

    /**
     * Creates new CollageTemplate, ready for Tile processing.<br>
     * This component will produce collage exactly with specified parameters.
     *
     * @param cols   Number of collage columns.
     * @param rows   Number of collage columns.
     * @param width  Collage width in pixels.
     * @param height Collage height in pixels.
     * @param items  List of media items. Items count must be = cols * rows, or error may occur
     */
    public BasicGridCollage(Integer cols, Integer rows, Integer width, Integer height, List<MediaDto> items) {

        imageBuilder = new ImageBuilder(width, height);

        GridCalculator gridCalc = new GridCalculator(cols, rows, width, height);
        int tileWidth = gridCalc.getTileWidth().intValue();
        int tileHeight = gridCalc.getTileHeight().intValue();

        Iterator<MediaDto> iterator = items.iterator();
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                MediaDto media = iterator.next();
                String url = media.getHighResUrl();
                Offset offset = gridCalc.getTileOffset(c, r);

                tiles.add(new BasicTile(tileWidth, tileHeight, offset, url));
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TileTemplate> getTiles() {
        return tiles;
    }

    /**
     * This implementation immediately adds tile's image to resulting collage.
     */
    @Override
    public void onTileResolved(TileTemplate tile) {
        imageBuilder.addImage(tile.getImage(), tile.getOffset());
    }

    /**
     * This implementation produces image in JPEG format
     */
    @Override
    public byte[] getBinaryImage() {
        return imageBuilder.getAsByteArray();
    }
}
