package com.uawebchallenge.collage.image.template;

import com.uawebchallenge.collage.image.Offset;
import javafx.scene.image.Image;

/**
 * Represents single tile inside a Collage.<br>
 * Used to properly arrange contents of collage.<br>
 * Instances shouldn't be instantiated directly, but rather by a CollageTemplate.
 *
 * @see com.uawebchallenge.collage.image.template.CollageTemplate
 */
public interface TileTemplate {

    /**
     * @return Width of tile in a collage
     */
    Integer getWidth();

    /**
     * @return Height of tile in a collage
     */
    Integer getHeight();

    /**
     * @return Tile offset from Collage zero point
     */
    Offset getOffset();

    /**
     * @return Image URL
     */
    String getUrl();

    /**
     * @return Actual image instance. May be null.
     */
    Image getImage();

    /**
     * @return true if tile's Image was successfully resolved.
     */
    boolean isResolved();

    /**
     * Perform Image resolve
     */
    void resolve();
}
