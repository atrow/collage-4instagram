package com.uawebchallenge.collage.image.template;

import java.util.List;

/**
 * Represents Collage which consists of separate images - tiles.<br>
 * This component relies on external worker to resolve tiles into actual images.
 * <p/>
 * After CollageTemplate creation, its tiles may be not resolved to actual image data.<br>
 * To resolve them, CollageTemplate exposes tiles list to external component, which will run image resolving task.<br>
 * On each tile resolve, onTileResolved() method should be called.
 * <p/>
 * After finishing tile job, getBinaryImage() should be called to produce Collage image.
 */
public interface CollageTemplate {

    /**
     * @return list of all Collage's tile templates
     */
    List<? extends TileTemplate> getTiles();

    /**
     * Run this method to notify CollageTemplate that tile contains actual Image data<br>
     * and is ready to processing.
     *
     * @param tile Tile which had been successfully resolved. If not resolved, error may happen.
     */
    void onTileResolved(TileTemplate tile);

    /**
     * Compose and produce actual Collage image in binary format.
     * @return image data as binary array.
     */
    byte[] getBinaryImage();

}
