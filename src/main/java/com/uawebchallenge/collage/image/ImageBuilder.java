package com.uawebchallenge.collage.image;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Binary image processor.
 * <p/>
 * After creation, contains empty rectangular canvas.<br>
 * Each call to addImage() prints image into specific point of canvas.<br>
 * After addition of all images, call getAsByteArray() or getAsOutputStream() to obtain resulted image.
 */
public class ImageBuilder {

    /**
     * Default image format
     */
    public static final String IMAGE_FORMAT_NAME = "jpeg";

    /**
     * Canvas where to print images
     */
    private final WritableImage canvas;

    /**
     * Create ImageBuilder for producing a resulting image of specific size.<br>
     * This size will be constrained.
     * @param width Resulting image width
     * @param height Resulting image height
     */
    public ImageBuilder(int width, int height) {
        canvas = new WritableImage(width, height);
    }

    /**
     * Adds an Image to specific point of canvas.
     * @param image    Image to add
     * @param offset Image offset from top left corner
     */
    public void addImage(Image image, Offset offset) {
        int width = Double.valueOf(image.getWidth()).intValue();
        int height = Double.valueOf(image.getHeight()).intValue();
        PixelReader pixelReader = image.getPixelReader();
        canvas.getPixelWriter().setPixels(offset.getX(), offset.getY(), width, height, pixelReader, 0, 0);
    }

    /**
     * Creates a binary representation of a resulting image.<br>
     * Uses JPEG encoding.
     * @return OutputStream for binary image data.
     */
    public ByteArrayOutputStream getAsOutputStream() {
        try {
            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(canvas, null);
            // Remove alpha-channel from buffered image.
            // Workaround for weird issue http://stackoverflow.com/questions/19548363/image-saved-in-javafx-as-jpg-is-pink-toned
            BufferedImage imageRGB = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.OPAQUE);
            Graphics2D graphics = imageRGB.createGraphics();
            graphics.drawImage(bufferedImage, 0, 0, null);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write(imageRGB, IMAGE_FORMAT_NAME, outputStream);
            graphics.dispose();
            return outputStream;
        } catch (IOException e) {
            throw new RuntimeException("Failed to create ", e);
        }
    }

    /**
     * Creates a binary representation of a resulting image.<br>
     * Uses JPEG encoding.
     * @return Byte array of binary image data.
     */
    public byte[] getAsByteArray() {
        return getAsOutputStream().toByteArray();
    }
}
