package com.uawebchallenge.collage.image;

/**
 * Represents integer offset by two coordinates.<br>
 * Used to define Tile offset in Collage.
 */
public class Offset {

    private final int x;

    private final int y;

    /**
     * @param x Horizontal offset
     * @param y Vertical offset
     */
    public Offset(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return Horizontal offset
     */
    public int getX() {
        return x;
    }

    /**
     * @return Vertical offset
     */
    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Offset{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
