package com.uawebchallenge.collage.config;

import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

/**
 * Spring Security java config.<br><br>
 * <p/>
 * <p>
 * Authentication is made externally by LoginController.<br>
 * While client is not authenticated, he can access:<br>
 * / - web root with welcome page<br>
 * /login - login redirect to Instagram Oauth2 <br>
 * /auth-result - redirect after Oauth2 authentication.
 * </p>
 * <p>
 * Requests to static contents are ignored, i.e. permitted to anyone.
 * </p>
 *
 * @see com.uawebchallenge.collage.controller.LoginController
 * @see com.uawebchallenge.collage.service.AuthService
 */
@Configuration
@EnableWebSecurity
@EnableWebMvcSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                // Turn on detailed Security debug info if appropriate debug level is set for Security package
                .debug(LoggerFactory.getLogger(WebSecurityConfigurerAdapter.class).isDebugEnabled())
                        // Ignore request to static resources such as CSS or JS files.
                .ignoring()
                .antMatchers(
                        "/static/**",
                        "/error/**",
                        "/css/**",
                        "/img/**",
                        "/js/**",
                        "/webjars/**",
                        "/fonts/**",
                        "/favicon.ico"
                );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .antMatcher("/**")
                        // Configures the logout function
                .logout()
                .deleteCookies("JSESSIONID")
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .and()
                        // Anyone can access the urls
                .authorizeRequests()
                .antMatchers(
                        "/",
                        "/login",
                        "/auth-result"
                )
                .permitAll()
                        // The rest of the our application is protected.
                .antMatchers("/**").authenticated()
                .and()
                        // Disable CSRF protection because no destructive behavior possible, and also for simplicity
                .csrf()
                .disable();
    }

}