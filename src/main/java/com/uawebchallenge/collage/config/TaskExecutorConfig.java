package com.uawebchallenge.collage.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Spring TaskExecutor configuration.
 * <p>
 * Task executors are used to gain control over concurrent calls to Instagram API.<br>
 * </p>
 * <i>
 * Implementation may be changed to throttle API load
 * </i>
 */
@Configuration
public class TaskExecutorConfig {

    @Bean(name = "imageFetching")
    public ThreadPoolTaskExecutor imageFetchingThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(8);
        executor.setMaxPoolSize(32);
        executor.setThreadNamePrefix("EXEC-IF-");
        executor.setWaitForTasksToCompleteOnShutdown(false);
        return executor;
    }

}