package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.service.CollageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Serves binary image data
 */
@Controller
public class ImageController {

    private static final Logger log = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    private CollageService collageService;

    /**
     * Returns resulting Collage image.
     * <p/>
     * Must be called after successful Collage creation task completion.
     * <p/>
     * <i>Such behavior allows JS Web UI to obtain a link for image download</i>
     * <p/>
     * <b>Request:</b> GET /collage/{taskId}
     *
     * @param taskId Id of complete Collage building task.
     * @return Image data in binary format, JPEG encoding.
     * @throws com.uawebchallenge.collage.exception.NotFoundException                          404 If Collage not found by taskId or expired
     * @throws com.uawebchallenge.collage.exception.BadRequestException                        400 if request parameters are wrong.
     * @throws org.springframework.security.authentication.InsufficientAuthenticationException 403 If user session has expired
     * @see ApiController#runCollage(com.uawebchallenge.collage.dto.CreateCollageRequestDto)
     */
    @ResponseBody
    @RequestMapping(value = "/collage/{taskId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getCollage(
            @PathVariable("taskId") String taskId) {

        log.debug("Requested Collage image by id: {}", taskId);
        return collageService.getCollage(taskId);
    }

}