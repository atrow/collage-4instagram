package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.dto.CollageBuildingTaskDto;
import com.uawebchallenge.collage.dto.CreateCollageRequestDto;
import com.uawebchallenge.collage.dto.MediaDto;
import com.uawebchallenge.collage.dto.UserInfoDto;
import com.uawebchallenge.collage.exception.InstagramAccessDeniedException;
import com.uawebchallenge.collage.security.UserInfo;
import com.uawebchallenge.collage.service.AuthService;
import com.uawebchallenge.collage.service.CollageService;
import com.uawebchallenge.collage.service.InstagramApiService;
import com.uawebchallenge.collage.service.ValidationService;
import org.jinstagram.exceptions.InstagramBadRequestException;
import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles REST API calls from the Web UI.
 * <p>
 * Communication format is JSON.<br>
 * All requests require user authentication.
 * </p>
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    private static final Logger log = LoggerFactory.getLogger(ApiController.class);

    /**
     * Minimal user search query length
     */
    public static final int MIN_SEARCH_LENGTH = 3;

    /**
     * Maximum user search results returned
     */
    public static final int MAX_SEARCH_USERS = 10;

    /**
     * Requests with item count greater than this value, should be rejected
     */
    public static final int MAX_INSTAGRAM_REQUEST_SIZE = 1024; // 32 * 32 collage

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AuthService authService;

    @Autowired
    private InstagramApiService instagramApiService;

    @Autowired
    private CollageService collageService;

    /**
     * Get recent media from the currently authenticated User's feed.<br>
     * (The media which User sees in his stripe)
     * <p/>
     * <b>Request:</b> GET /api/media/feed
     *
     * @param count   Desired size of media list. Method may return less items, if requested amount is not available.
     *                max value = MAX_INSTAGRAM_REQUEST_SIZE
     * @param request Current HTTP request wrapper.
     * @return List of media item representations.
     * @throws org.jinstagram.exceptions.InstagramException                                    500 If Instagram API call fails
     * @throws com.uawebchallenge.collage.exception.BadRequestException                        400 if request parameters are wrong.
     * @throws org.springframework.security.authentication.InsufficientAuthenticationException 403 If user session has expired
     * @see com.uawebchallenge.collage.dto.MediaDto
     */
    @RequestMapping("/media/feed")
    public List<MediaDto> getMediaFeed(
            @RequestParam(value = "count", required = false, defaultValue = "16") Integer count,
            WebRequest request) throws InstagramException {

        log.debug("Requested {} items from MediaFeed", count);
        validationService.validateRequestLimit(count, MAX_INSTAGRAM_REQUEST_SIZE);

        UserInfo userInfo = authService.getUserInfo(request);
        return instagramApiService.getMediaFeed(userInfo.getToken(), count);
    }

    /**
     * Get recent media posted by the User with specific Id<br>
     * (This may be media owned by authenticated User, or by any other user)
     * <p/>
     * <b>WARNING, Viewing of some user's feed may be prohibited.</b>
     * <p/>
     * <b>Request:</b> GET /api/media/recent
     *
     * @param count   Desired size of media list. Method may return less items, if requested amount is not available.
     *                max value = MAX_INSTAGRAM_REQUEST_SIZE
     * @param userId  Instagram User Id, numeric string
     * @param request Current HTTP request wrapper.
     * @return List of media item representations.
     * @throws org.jinstagram.exceptions.InstagramException                                    500 If Instagram API call fails
     * @throws com.uawebchallenge.collage.exception.InstagramAccessDeniedException             405 If access to user's feed is denied
     * @throws com.uawebchallenge.collage.exception.BadRequestException                        400 if request parameters are wrong.
     * @throws org.springframework.security.authentication.InsufficientAuthenticationException 403 If user session has expired
     * @see com.uawebchallenge.collage.dto.MediaDto
     */
    @RequestMapping("/media/recent")
    public List<MediaDto> getMediaUser(
            @RequestParam(value = "count", required = false, defaultValue = "16") Integer count,
            @RequestParam("userId") String userId,
            WebRequest request) throws InstagramException {

        log.debug("Requested {} items from MediaUser", count);
        validationService.validateRequestLimit(count, MAX_INSTAGRAM_REQUEST_SIZE);

        UserInfo userInfo = authService.getUserInfo(request);

        try {
            return instagramApiService.getMediaUser(userInfo.getToken(), userId, count);
        } catch (InstagramBadRequestException e) {
            log.warn("Instagram responded with error", e);
            if (e.getMessage().contains("NotAllowed")) {
                throw new InstagramAccessDeniedException(e);
            }
            throw e;
        }
    }

    /**
     * Search Instagram users by specified username's fragment
     * <p/>
     * <b>Request:</b> GET /api/users/search
     *
     * @param search  Username search string, must not be less than MIN_SEARCH_LENGTH.
     *                Else, empty response is returned.
     * @param request Current HTTP request wrapper.
     * @return List of matching user names and user ids
     * @throws org.jinstagram.exceptions.InstagramException                                    500 If Instagram API call fails
     * @throws org.springframework.security.authentication.InsufficientAuthenticationException 403 If user session has expired
     * @see com.uawebchallenge.collage.dto.UserInfoDto
     */
    @RequestMapping(value = "/users/search")
    public List<UserInfoDto> searchUsers(
            @RequestParam(value = "search", required = false) String search,
            WebRequest request) throws InstagramException {

        log.debug("Requested user search by string: {}", search);
        if (!StringUtils.hasText(search) || search.length() < MIN_SEARCH_LENGTH) {
            return new ArrayList<>();
        }

        UserInfo userInfo = authService.getUserInfo(request);
        return instagramApiService.searchUsers(userInfo.getToken(), search, MAX_SEARCH_USERS);
    }

    /**
     * Synchronously run Collage creation job
     * <p/>
     * Client must specify collage columns, rows, pixel width, height, and list of media items.<br>
     * Collage is created synchronously, and client receives job status and id.<br>
     * By this id, client then requests binary image for download (via ImageController)
     * <p/>
     * <i>Such behavior allows JS Web UI to obtain a link for image download</i>
     * <p/>
     * <b>Request:</b> POST /api/collage
     *
     * @param dto see CreateCollageRequestDto
     * @return Collage job id and status
     * @throws com.uawebchallenge.collage.exception.BadRequestException                        400 if request parameters are wrong.
     * @throws org.springframework.security.authentication.InsufficientAuthenticationException 403 If user session has expired
     * @see com.uawebchallenge.collage.dto.CreateCollageRequestDto
     * @see com.uawebchallenge.collage.dto.CollageBuildingTaskDto
     * @see com.uawebchallenge.collage.dto.MediaDto
     * @see com.uawebchallenge.collage.image.CollageBuildingTask.Status
     * @see ImageController#getCollage(java.lang.String)
     */
    @RequestMapping(value = "/collage", method = RequestMethod.POST)
    public CollageBuildingTaskDto runCollage(
            @Valid @RequestBody CreateCollageRequestDto dto,
            WebRequest request) {

        log.debug("Requested Collage creation: {}", dto);
        authService.getUserInfo(request);

        return collageService.runCollageCreation(dto.getCols(), dto.getRows(), dto.getWidth(), dto.getHeight(), dto.getItems());
    }

    /**
     * For testing
     */
    public void setInstagramApiService(InstagramApiService instagramApiService) {
        this.instagramApiService = instagramApiService;
    }
}