package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.security.UserInfo;
import com.uawebchallenge.collage.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

/**
 * Serves requests to web root (welcome page)
 */
@Controller
public class HomeController {

    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private AuthService authService;

    /**
     * Handle root request
     * <p/>
     * If user is already authenticated with Instagram, load application page.<br>
     * If not, show Welcome page.
     * <p/>
     * <b>Request:</b> GET /
     *
     * @param request   Current HTTP request wrapper.
     * @param model     Model to initially populate Web app view with current User's info
     * @return Welcome page or app page, if user is already authenticated
     */
    @RequestMapping("/")
    public String getIndex(WebRequest request, Model model) {

        UserInfo userInfo = authService.getUserInfoOrNull(request);
        if (userInfo == null) {
            log.debug("User is unauthorized, serving welcome page");
            return "index";
        }

        log.debug("User authorized successfully: {}", userInfo.getName());
        log.debug("Serving app page");

        model.addAttribute("userName", userInfo.getName());
        model.addAttribute("userId", userInfo.getId());

        return "app";
    }

}