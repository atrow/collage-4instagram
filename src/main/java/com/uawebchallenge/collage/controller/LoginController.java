package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.exception.Oauth2Exception;
import com.uawebchallenge.collage.service.AuthService;
import com.uawebchallenge.collage.service.InstagramApiService;
import org.jinstagram.auth.model.Token;
import org.jinstagram.entity.users.basicinfo.UserInfoData;
import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Handles User's Oauth2 authentication via Instagram
 */
@Controller
public class LoginController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private InstagramApiService instagramApiService;

    @Autowired
    private AuthService authService;

    /**
     * Serve login request.
     * <p/>
     * If user is not authenticated, redirect to Instagram Oauth2 URL.<br>
     * If user is already authenticated, redirect to '/' and he will obtain Web UI.
     * <p/>
     * <b>Request:</b> GET /login
     *
     * @param request Current HTTP request wrapper.
     * @return redirection to '/' or authUrl.
     * @see com.uawebchallenge.collage.service.InstagramApiService#getAuthUrl()
     */
    @RequestMapping("/login")
    public RedirectView login(WebRequest request) {

        if (authService.getUserInfoOrNull(request) != null) {
            log.debug("Already authenticated user who was trying to login");
            return new RedirectView("/");
        }

        String authUrl = instagramApiService.getAuthUrl();
        log.debug("Authenticating with auth URL: {}", authUrl);

        return new RedirectView(authUrl);
    }

    /**
     * Process the Successful authentication callback from an Instagram OAuth 2.
     * <p/>
     * After receiving auth code, obtains Instagram access-token, basic User info, <br>
     * and uses them for authentication via AuthService
     * <p/>
     * <b>Request:</b> GET /auth-result?code={code}
     *
     * @param code    Oauth2 response authentication code.
     * @param request Current HTTP request wrapper.
     * @return redirection to '/'
     * @throws org.jinstagram.exceptions.InstagramException 500 If Instagram API call fails
     * @see com.uawebchallenge.collage.service.AuthService#authenticate(org.springframework.web.context.request.WebRequest, com.uawebchallenge.collage.security.UserInfo)
     */
    @RequestMapping(value = "/auth-result", params = "code")
    public RedirectView oauth2SuccessCallback(@RequestParam("code") String code, WebRequest request) throws InstagramException {
        log.debug("User successfully authenticated on Instagram");

        Token token = instagramApiService.getAccessTokenByOauthResponseCode(code);
        UserInfoData instagramUserInfo = instagramApiService.getCurrentUserInfo(token);

        authService.authenticate(request, instagramUserInfo.getId(), instagramUserInfo.getUsername(), token);
        log.debug("User authentication complete");

        return new RedirectView("/");
    }

    /**
     * Process the Error authentication callback from an Instagram OAuth 2.
     * <p/>
     * Throws the exception so user will be redirected to Error view and read error info.
     * <p/>
     * <b>Request:</b> GET /auth-result?error={error}
     *
     * @param error            Error status returned by Instagram
     * @param errorDescription Error description possibly returned by Instagram
     * @throws com.uawebchallenge.collage.exception.Oauth2Exception 401 As result of request
     */
    @RequestMapping(value = "/auth-result", params = "error")
    public void oauth2ErrorCallback(
            @RequestParam("error") String error,
            @RequestParam(value = "error_description", required = false) String errorDescription) {

        log.warn("Error during authorization: {}: {}", error, errorDescription);

        if (errorDescription != null) {
            error += ": " + errorDescription;
        }

        throw new Oauth2Exception(error);
    }

    /**
     * Process the authentication callback when code parameter is not given, maybe user denied authorization.
     * Redirects to web root
     * @return redirection to '/'
     */
    @RequestMapping(value = "/auth-result")
    public RedirectView canceledAuthorizationCallback() {
        log.info("Looks like user declined authorization");
        return new RedirectView("/");
    }

    /**
     * For testing
     */
    public void setInstagramApiService(InstagramApiService instagramApiService) {
        this.instagramApiService = instagramApiService;
    }
}