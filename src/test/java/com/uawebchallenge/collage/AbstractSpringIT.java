package com.uawebchallenge.collage;

import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Root class for all Integration tests, both MockMVC and full server start
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CollageMakerApplication.class)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
public abstract class AbstractSpringIT {
    // Empty
}
