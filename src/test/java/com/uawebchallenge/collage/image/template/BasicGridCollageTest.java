package com.uawebchallenge.collage.image.template;

import com.uawebchallenge.collage.dto.MediaDto;
import com.uawebchallenge.collage.service.JavaFxStarterService;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * BasicGridCollageTest
 */
public class BasicGridCollageTest {

    public static final String HTTP_LOW = "http://low";
    public static final String HTTP_HIGH = "http://high";

    @BeforeClass
    public static void initJavaFX() throws Exception {
        JavaFxStarterService service = new JavaFxStarterService();
        service.afterPropertiesSet();
    }

    @Test
    public void
    should_compose_valid_grid_simple() throws Exception {

        BasicGridCollage collage = new BasicGridCollage(10, 10, 100, 100, getMediaItems(100));

        assertThat(collage.getTiles().size(), equalTo(100));

        for (TileTemplate tile : collage.getTiles()) {
            assertThat(tile.getWidth(), equalTo(10));
            assertThat(tile.getHeight(), equalTo(10));
            assertThat(tile.getUrl(), equalTo(HTTP_HIGH));
            assertThat(tile.isResolved(), equalTo(false));
        }
    }

    @Test
    public void
    should_compose_valid_grid_decimals() throws Exception {

        BasicGridCollage collage = new BasicGridCollage(3, 7, 100, 100, getMediaItems(21));

        assertThat(collage.getTiles().size(), equalTo(21));

        for (TileTemplate tile : collage.getTiles()) {
            assertThat(tile.getWidth(), equalTo(33));
            assertThat(tile.getHeight(), equalTo(14));
        }
    }

    @Test
    public void
    should_produce_blank_image_if_no_tiles_resolved() throws Exception {

        BasicGridCollage collage = new BasicGridCollage(2, 2, 10, 10, getMediaItems(4));

        byte[] binary = collage.getBinaryImage();

        Resource resource = new ClassPathResource("images/black10x10.jpg");
        byte[] reference = IOUtils.toByteArray(resource.getInputStream());

        assertThat(binary.length, equalTo(reference.length));

        for (int i = 0; i < binary.length; i++) {
            assertTrue(binary[i] == reference[i]);
        }
    }

    @Test
    public void
    should_create_valid_tile() throws Exception {

        List<MediaDto> items = new ArrayList<>();
        items.add(new MediaDto("1", "2", "3"));
        BasicGridCollage collage = new BasicGridCollage(1, 1, 10, 10, items);

        assertThat(collage.getTiles().get(0).toString(), equalTo("BasicTile{width=10, height=10, offset=Offset{x=0, y=0}, url='3'}"));
    }

    @Test
    public void
    should_produce_collage_if_all_tiles_resolved() throws Exception {

        List<MediaDto> items = new ArrayList<>();
        items.add(new MediaDto("1", "images/blue10x10.jpg", "images/blue10x10.jpg"));
        items.add(new MediaDto("2", "images/yellow10x10.jpg", "images/yellow10x10.jpg"));
        items.add(new MediaDto("3", "images/green10x10.jpg", "images/green10x10.jpg"));
        items.add(new MediaDto("4", "images/red10x10.jpg", "images/red10x10.jpg"));

        BasicGridCollage collage = new BasicGridCollage(2, 2, 20, 20, items);

        for (TileTemplate tile : collage.getTiles()) {
            tile.resolve();
            collage.onTileResolved(tile);
            assertThat(tile.isResolved(), equalTo(true));
        }

        Resource resource = new ClassPathResource("images/bygr_tiles.jpg");
        byte[] reference = IOUtils.toByteArray(resource.getInputStream());

        byte[] binary = collage.getBinaryImage();

        assertThat(binary.length, equalTo(reference.length));

        for (int i = 0; i < binary.length; i++) {
            assertTrue(binary[i] == reference[i]);
        }
    }

    @Test(expected = Exception.class)
    public void
    should_throw_exception_when_wrong_media_count() throws Exception {
        new BasicGridCollage(3, 3, 100, 100, getMediaItems(8));
    }

    @Test(expected = Exception.class)
    public void
    should_throw_exception_when_wrong_image_size() throws Exception {
        new BasicGridCollage(3, 3, 100, 0, getMediaItems(9));
    }

    private List<MediaDto> getMediaItems(int count) {

        List<MediaDto> items = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            items.add(new MediaDto(Integer.toString(i), HTTP_LOW, HTTP_HIGH));
        }
        return items;

    }

}
