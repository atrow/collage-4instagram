package com.uawebchallenge.collage.image.template;

import com.uawebchallenge.collage.exception.BadRequestException;
import com.uawebchallenge.collage.service.ValidationService;
import org.junit.Test;

import java.util.Collections;

/**
 * ValidationServiceTest
 */
public class ValidationServiceTest {

    private static ValidationService validationService = new ValidationService();

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_NULL_validated_for_GreaterThanZero() throws Exception {
        validationService.validateGreaterThanZero(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_0_validated_for_GreaterThanZero() throws Exception {
        validationService.validateGreaterThanZero(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_minus1_validated_for_GreaterThanZero() throws Exception {
        validationService.validateGreaterThanZero(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_minusMAX_VALUE_validated_for_GreaterThanZero() throws Exception {
        validationService.validateGreaterThanZero(-Integer.MAX_VALUE);
    }

    @Test
    public void
    should_pass_when_1_validated_for_GreaterThanZero() throws Exception {
        validationService.validateGreaterThanZero(1);
    }

    @Test
    public void
    should_pass_when_MAX_VALUE_validated_for_GreaterThanZero() throws Exception {
        validationService.validateGreaterThanZero(Integer.MAX_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_NULL_validated_for_NotNull() throws Exception {
        validationService.validateNotNull(null);
    }

    @Test
    public void
    should_pass_when_string_validated_for_NotNull() throws Exception {
        validationService.validateNotNull("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_empty_string_validated_for_NotEmpty() throws Exception {
        validationService.validateNotEmpty("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_spaces_string_validated_for_NotEmpty() throws Exception {
        validationService.validateNotEmpty("   ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_NULL_validated_for_NotEmpty() throws Exception {
        validationService.validateNotEmpty(null);
    }

    @Test
    public void
    should_pass_when_string_validated_for_NotEmpty() throws Exception {
        validationService.validateNotEmpty("string");
    }

    @Test
    public void
    should_pass_when_string_with_spaces_validated_for_NotEmpty() throws Exception {
        validationService.validateNotEmpty("  string  ");
    }

    @Test(expected = BadRequestException.class)
    public void
    should_throw_BadRequestException_when_empty_collection_validated_for_CollectionSize_1() throws Exception {
        validationService.validateCollectionSize(Collections.EMPTY_LIST, 1);
    }

    @Test
    public void
    should_pass_when_empty_collection_validated_for_CollectionSize_0() throws Exception {
        validationService.validateCollectionSize(Collections.EMPTY_LIST, 0);
    }

    @Test(expected = BadRequestException.class)
    public void
    should_throw_BadRequestException_when_empty_collection_validated_for_CollectionSize_minus1() throws Exception {
        validationService.validateCollectionSize(Collections.EMPTY_LIST, -1);
    }

    @Test(expected = BadRequestException.class)
    public void
    should_throw_BadRequestException_when_nonzero_collection_validated_for_CollectionSize_0() throws Exception {
        validationService.validateCollectionSize(Collections.singleton(""), 0);
    }

    @Test
    public void
    should_pass_when_nonzero_collection_validated_for_CollectionSize_1() throws Exception {
        validationService.validateCollectionSize(Collections.singleton(""), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_NULL_to_validateRequestLimit() throws Exception {
        validationService.validateRequestLimit(null, 0);
    }

    @Test(expected = BadRequestException.class)
    public void
    should_throw_BadRequestException_when_request_limit_exceeded_0() throws Exception {
        validationService.validateRequestLimit(10, 0);
    }

    @Test(expected = BadRequestException.class)
    public void
    should_throw_BadRequestException_when_request_limit_exceeded_minus1() throws Exception {
        validationService.validateRequestLimit(0, -1);
    }

    @Test(expected = BadRequestException.class)
    public void
    should_throw_BadRequestException_when_request_limit_exceeded_MAX() throws Exception {
        validationService.validateRequestLimit(Integer.MAX_VALUE, Integer.MAX_VALUE - 1);
    }

    @Test
    public void
    should_pass_when_validateRequestLimit_ok() throws Exception {
        validationService.validateRequestLimit(Integer.MAX_VALUE - 1, Integer.MAX_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_IllegalArgumentException_when_validating_invalid_url() throws Exception {
        validationService.validateIsUrl("illegal");
    }

    @Test
    public void
    should_pass_when_validating_valid_url() throws Exception {
        validationService.validateIsUrl("HTTP:///");
    }

}
