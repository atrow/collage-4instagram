package com.uawebchallenge.collage.image;

import com.uawebchallenge.collage.image.template.TileTemplate;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * TileResolveTaskTest
 */
public class TileResolveTaskTest {

    @Test
    public void
    should_resolve_tile_template_on_call() throws Exception {

        TileTemplate template = mock(TileTemplate.class);
        TileResolveTask task = new TileResolveTask(template);

        TileTemplate result = task.call();

        verify(template, times(1)).resolve();

        assertThat(result, not(nullValue()));
        assertThat(result, sameInstance(template));
    }

}
