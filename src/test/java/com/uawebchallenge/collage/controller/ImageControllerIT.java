package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.AbstractMockMvcIT;
import com.uawebchallenge.collage.dto.CollageBuildingTaskDto;
import com.uawebchallenge.collage.dto.MediaDto;
import com.uawebchallenge.collage.service.CollageService;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * HomeControllerIT
 */
public class ImageControllerIT extends AbstractMockMvcIT {

    private static final Logger log = LoggerFactory.getLogger(ImageControllerIT.class);

    @Autowired
    private CollageService collageService;

    @Test
    public void
    should_serve_image_if_exists() throws Exception {

        MediaDto dto = new MediaDto("", "images/black10x10.jpg", "images/black10x10.jpg");
        CollageBuildingTaskDto result = collageService.runCollageCreation(1, 1, 10, 10, Collections.singletonList(dto));
        assertThat(result, not(nullValue()));
        assertThat(result.getId(), not(nullValue()));

        byte[] response = mvc.perform(get("/collage/{taskId}", result.getId()))
                .andExpect(status().is(HttpServletResponse.SC_OK))
                .andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
                .andReturn().getResponse().getContentAsByteArray();

        // TODO: Need a Matcher for image comparison
        Resource resource = new ClassPathResource("images/black10x10.jpg");
        byte[] reference = IOUtils.toByteArray(resource.getInputStream());

        assertThat(response.length, equalTo(reference.length));
        for (int i = 0; i < response.length; i++) {
            assertTrue(response[i] == reference[i]);
        }
    }

    @Test
    public void
    should_return_404_if_task_not_exist() throws Exception {

        mvc.perform(get("/collage/{taskId}", "e676876c-3b8e-4cb9-ae10-46858ae5dd54"))
                .andExpect(status().is(HttpServletResponse.SC_NOT_FOUND));

    }


}
