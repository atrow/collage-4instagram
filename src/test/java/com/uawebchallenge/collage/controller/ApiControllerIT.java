package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.AbstractMockMvcIT;
import com.uawebchallenge.collage.service.AuthService;
import com.uawebchallenge.collage.service.InstagramApiService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.util.NestedServletException;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * LoginControllerIT
 */
public class ApiControllerIT extends AbstractMockMvcIT {

    private static InstagramApiService instagramApiServiceMock = mock(InstagramApiService.class);

    @Autowired
    ApiController apiController;

    @Test(expected = InsufficientAuthenticationException.class)
    public void
    should_throw_error_if_user_unauthorised_on_getMediaFeed() throws Throwable {
        try {
            mvc.perform(get("/api/media/feed"));
        } catch (NestedServletException e) {
            throw e.getCause();
        }
    }

    @Test
    public void
    should_throw_error_if_count_limit_exceeded_on_getMediaFeed() throws Exception {

        mvc.perform(get("/api/media/feed")
                        .param("count", "1025")
                        .principal(getPrincipal())
                        .sessionAttr(AuthService.USER_INFO, getUserInfo())
        )
                .andExpect(status().is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void
    should_throw_error_if_count_limit_exceeded_on_getMediaUser() throws Exception {

        mvc.perform(get("/api/media/recent")
                        .param("count", "1025")
                        .principal(getPrincipal())
                        .sessionAttr(AuthService.USER_INFO, getUserInfo())
        )
                .andExpect(status().is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test(expected = InsufficientAuthenticationException.class)
    public void
    should_throw_error_if_user_unauthorised_on_getMediaUser() throws Throwable {
        try {
            mvc.perform(get("/api/media/recent")
                    .param("userId", "id"));
        } catch (NestedServletException e) {
            throw e.getCause();
        }
    }

    @Test(expected = InsufficientAuthenticationException.class)
    public void
    should_throw_error_if_user_unauthorised_on_searchUsers() throws Throwable {
        try {
            mvc.perform(get("/api/users/search")
                    .param("search", "abc"));
        } catch (NestedServletException e) {
            throw e.getCause();
        }
    }

    @Test
    public void
    should_return_empty_array_on_searchUser_length_less_3() throws Throwable {
        String response = mvc.perform(get("/api/users/search")
                        .param("search", "ab")
        )
                .andExpect(status().is(HttpServletResponse.SC_OK))
                .andReturn().getResponse().getContentAsString();
        assertThat(response, equalTo("[]"));
    }

}
