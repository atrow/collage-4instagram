package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.AbstractMockMvcIT;
import com.uawebchallenge.collage.service.AuthService;
import com.uawebchallenge.collage.service.InstagramApiService;
import org.jinstagram.auth.model.Token;
import org.jinstagram.entity.users.basicinfo.UserInfoData;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * LoginControllerIT
 */
@DirtiesContext
public class LoginControllerIT extends AbstractMockMvcIT {

    private static InstagramApiService instagramApiServiceMock = mock(InstagramApiService.class);

    @Autowired LoginController loginController;

    @Test
    public void
    should_redirect_already_authorized_user_to_root_on_login() throws Exception {

        mvc.perform(get("/login")
                .principal(getPrincipal())
                .sessionAttr(AuthService.USER_INFO, getUserInfo()))
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void
    should_redirect_authorized_user_to_Oauth2_redirect_url() throws Exception {

        loginController.setInstagramApiService(instagramApiServiceMock);

        when(instagramApiServiceMock.getAuthUrl())
                .thenReturn("the_url");

        mvc.perform(get("/login"))
                .andExpect(redirectedUrl("the_url"));
    }

    @Test
    public void
    should_authenticate_user_on_successful_Oauth2_callback() throws Exception {

        loginController.setInstagramApiService(instagramApiServiceMock);

        Token tokenMock = mock(Token.class);
        UserInfoData instagramUserInfoMock = mock(UserInfoData.class);

        when(instagramUserInfoMock.getId())
                .thenReturn("id");

        when(instagramUserInfoMock.getUsername())
                .thenReturn("username");

        when(instagramApiServiceMock.getAccessTokenByOauthResponseCode(eq("success_code")))
                .thenReturn(tokenMock);

        when(instagramApiServiceMock.getCurrentUserInfo(eq(tokenMock)))
                .thenReturn(instagramUserInfoMock);

        mvc.perform(get("/auth-result")
                .param("code", "success_code")
        )
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void
    should_throw_unauthorized_exception_on_unsuccessful_Oauth2_callback() throws Exception {

        mvc.perform(get("/auth-result")
                        .param("error", "error")
        )
                .andExpect(status().is(HttpServletResponse.SC_UNAUTHORIZED));
    }

}
