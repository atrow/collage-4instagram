package com.uawebchallenge.collage.controller;

import com.uawebchallenge.collage.AbstractMockMvcIT;
import com.uawebchallenge.collage.service.AuthService;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * HomeControllerIT
 */
public class HomeControllerIT extends AbstractMockMvcIT {

    @Test
    public void
    should_serve_welcome_page_for_unauthorized_user() throws Exception {
        String response = mvc.perform(get("/"))
                .andExpect(status().is(HttpServletResponse.SC_OK))
                .andReturn().getResponse().getContentAsString();

        assertThat(response, containsString("<title>Instagram Collage</title>"));
        assertThat(response, containsString("href=\"/login\">"));
    }

    @Test
    public void
    should_serve_app_page_for_authorized_user() throws Exception {
        String response = mvc.perform(get("/")
                .principal(getPrincipal())
                .sessionAttr(AuthService.USER_INFO, getUserInfo())
        )
                .andExpect(status().is(HttpServletResponse.SC_OK))
                .andReturn().getResponse().getContentAsString();

        assertThat(response, containsString("<body ng-app=\"collage\">"));
    }

}
