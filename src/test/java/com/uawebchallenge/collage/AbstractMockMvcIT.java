package com.uawebchallenge.collage;

import com.uawebchallenge.collage.security.GrantedAuthoritiesProvider;
import com.uawebchallenge.collage.security.UserInfo;
import com.uawebchallenge.collage.service.AuthService;
import org.jinstagram.auth.model.Token;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.security.Principal;

import static org.mockito.Mockito.*;

public abstract class AbstractMockMvcIT extends AbstractSpringIT {

    /**
     * Main instance of the web application context
     */
    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    private AuthService authService;

    /**
     * Performer of all requests
     */
    protected MockMvc mvc;

    @Before
    public void before() {
        mvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    protected Principal getPrincipal() {

        Principal principal = new Principal() {
            @Override
            public String getName() {
                return "";
            }
        };

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("id", "token", GrantedAuthoritiesProvider.getDefault());
        SecurityContextHolder.getContext().setAuthentication(auth);

        return principal;
    }

    protected UserInfo getUserInfo() {
        return new UserInfo("id", "name", mock(Token.class));
    }


}
