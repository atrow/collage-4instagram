package com.uawebchallenge.collage.service;

import com.uawebchallenge.collage.AbstractSpringIT;
import com.uawebchallenge.collage.dto.CollageBuildingTaskDto;
import com.uawebchallenge.collage.dto.MediaDto;
import com.uawebchallenge.collage.image.CollageBuildingTask;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * CollageServiceIT
 */
public class CollageServiceIT extends AbstractSpringIT {

    @Autowired
    private CollageService collageService;

    private static List<MediaDto> items;

    private static MediaDto dtoMock;

    @BeforeClass
    public static void before() {
        dtoMock = mock(MediaDto.class);
        items = new ArrayList<>();
        items.add(dtoMock);
        items.add(dtoMock);
        items.add(dtoMock);
        items.add(dtoMock);
    }

    @Test
    public void
    should_run_collage_creation() throws Exception {

        when(dtoMock.getHighResUrl()).thenReturn("images/black10x10.jpg");

        CollageBuildingTaskDto result = collageService.runCollageCreation(2, 2, 10, 10, items);

        assertThat(result, not(nullValue()));
        assertThat(result.getStatus(), equalTo(CollageBuildingTask.Status.DONE));
        assertThat(result.getId(), not(isEmptyString()));
    }

    @Test
    public void
    should_produce_valid_collage() throws Exception {

        when(dtoMock.getHighResUrl()).thenReturn("images/black10x10.jpg");

        CollageBuildingTaskDto result = collageService.runCollageCreation(2, 2, 10, 10, items);

        byte[] collage = collageService.getCollage(result.getId());

        Resource resource = new ClassPathResource("images/black10x10.jpg");
        byte[] reference = IOUtils.toByteArray(resource.getInputStream());

        assertThat(collage.length, equalTo(reference.length));

        for (int i = 0; i < collage.length; i++) {
            assertTrue(collage[i] == reference[i]);
        }
    }

    @Test(expected = Exception.class)
    public void
    should_throw_exception_if_image_not_resolved() throws Exception {

        when(dtoMock.getHighResUrl()).thenReturn("");

        collageService.runCollageCreation(2, 2, 10, 10, items);
    }

}
