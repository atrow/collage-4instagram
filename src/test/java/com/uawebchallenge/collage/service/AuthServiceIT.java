package com.uawebchallenge.collage.service;

import com.uawebchallenge.collage.AbstractSpringIT;
import com.uawebchallenge.collage.security.GrantedAuthoritiesProvider;
import com.uawebchallenge.collage.security.UserInfo;
import org.jinstagram.auth.model.Token;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

/**
 * AuthServiceIT
 */
public class AuthServiceIT extends AbstractSpringIT {

    @Autowired
    private AuthService authService;

    @Test
    public void
    should_put_authentication_in_SecurityContext_when_authenticated() throws Exception {

        WebRequest webRequestMock = mock(WebRequest.class);
        Token tokenMock = mock(Token.class);

        authService.authenticate(webRequestMock, "userId", "userName", tokenMock);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        assertThat(auth, not(nullValue()));
        assertTrue(auth.isAuthenticated());
        assertThat(auth.getAuthorities().size(), equalTo(1));
        assertThat(auth.getAuthorities().iterator().next(), equalTo(GrantedAuthoritiesProvider.getDefault().get(0)));
        assertThat(auth.getPrincipal(), equalTo("userId"));
    }

    @Test
    public void
    should_put_UserInfo_in_Session_when_authenticated() throws Exception {

        WebRequest webRequestMock = mock(WebRequest.class);
        Token tokenMock = mock(Token.class);

        authService.authenticate(webRequestMock, "userId", "userName", tokenMock);

        doAnswer(new Answer() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                String name = (String) invocation.getArguments()[0];
                UserInfo userInfo = (UserInfo) invocation.getArguments()[1];

                assertThat(name, equalTo(AuthService.USER_INFO));
                assertThat(userInfo, not(nullValue()));
                assertThat(userInfo.getId(), equalTo("userId"));
                assertThat(userInfo.getName(), equalTo("userName"));
                assertThat(userInfo.getToken(), sameInstance(tokenMock));

                return null;
            }
        }).when(webRequestMock).setAttribute(anyString(), any(), anyInt());
    }

    @Test
    public void
    should_get_UserInfo_after_authentication() throws Exception {

        WebRequest webRequestMock = mock(WebRequest.class);
        Token tokenMock = mock(Token.class);

        final UserInfo[] userInfos = new UserInfo[1];

        doAnswer(new Answer() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                userInfos[0] = (UserInfo) invocation.getArguments()[1];
                return null;
            }
        }).when(webRequestMock).setAttribute(anyString(), any(), anyInt());

        authService.authenticate(webRequestMock, "userId", "userName", tokenMock);
        assertThat(userInfos[0], not(nullValue()));

        when(webRequestMock.getAttribute(eq(AuthService.USER_INFO), eq(RequestAttributes.SCOPE_SESSION)))
                .thenReturn(userInfos[0]);


        UserInfo result = authService.getUserInfoOrNull(webRequestMock);

        assertThat(result, equalTo(userInfos[0]));
    }

    @Test
    public void
    should_remove_UserInfo_on_de_authentication() throws Exception {
        WebRequest webRequestMock = mock(WebRequest.class);

        authService.deAuthenticate(webRequestMock);

        verify(webRequestMock, times(1)).removeAttribute(AuthService.USER_INFO, RequestAttributes.SCOPE_SESSION);
    }

    @Test
    public void
    should_remove_Session_info_on_de_authentication() throws Exception {

        WebRequest webRequestMock = mock(WebRequest.class);
        Token tokenMock = mock(Token.class);

        authService.authenticate(webRequestMock, "userId", "userName", tokenMock);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        assertThat(auth, not(nullValue()));
        assertTrue(auth.isAuthenticated());

        authService.deAuthenticate(webRequestMock);

        auth = SecurityContextHolder.getContext().getAuthentication();
        assertThat(auth, nullValue());
    }

}
