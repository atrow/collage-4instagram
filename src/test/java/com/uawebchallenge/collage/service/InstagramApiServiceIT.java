package com.uawebchallenge.collage.service;

import com.uawebchallenge.collage.AbstractSpringIT;
import org.jinstagram.auth.model.Token;
import org.jinstagram.auth.oauth.InstagramService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * InstagramApiServiceIT
 */
@DirtiesContext
public class InstagramApiServiceIT extends AbstractSpringIT {

    private static InstagramService instagramServiceMock = mock(InstagramService.class);

    @Autowired
    private InstagramApiService instagramApiService;

    @Before
    public void before() {
        instagramApiService.setService(instagramServiceMock);
    }

    @Test
    public void
    should_try_to_get_access_token_by_code() throws Exception {
        Token tokenMock = mock(Token.class);

        when(instagramServiceMock.getAccessToken(any(), any()))
                .thenReturn(tokenMock);

        Token result = instagramApiService.getAccessTokenByOauthResponseCode("code");

        assertThat(result, sameInstance(tokenMock));
        verify(instagramServiceMock.getAccessToken(any(), any()), times(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void
    should_throw_exception_when_get_access_token_by_empty_code() throws Exception {
        instagramApiService.getAccessTokenByOauthResponseCode(" ");
    }

}
