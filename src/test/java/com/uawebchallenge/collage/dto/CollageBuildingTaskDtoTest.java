package com.uawebchallenge.collage.dto;

import com.uawebchallenge.collage.image.CollageBuildingTask;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * CollageBuildingTaskDtoTest
 */
public class CollageBuildingTaskDtoTest {

    @Test
    public void
    should_create_dto_from_valid_task() {

        CollageBuildingTask task = new CollageBuildingTask();
        CollageBuildingTaskDto dto = CollageBuildingTaskDto.fromTask(task);

        assertThat(dto, not(nullValue()));
        assertThat(dto.getId(), equalTo(task.getId()));
        assertThat(dto.getStatus(), equalTo(task.getStatus()));
    }

    @Test
    public void
    should_create_dto_from_null_task() {

        CollageBuildingTaskDto dto = CollageBuildingTaskDto.fromTask(null);

        assertThat(dto, not(nullValue()));
        assertThat(dto.getId(), nullValue());
        assertThat(dto.getStatus(), equalTo(CollageBuildingTask.Status.EXPIRED));
    }

}
