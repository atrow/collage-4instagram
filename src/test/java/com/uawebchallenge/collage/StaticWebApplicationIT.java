package com.uawebchallenge.collage;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@IntegrationTest("server.port=0")
@DirtiesContext
public class StaticWebApplicationIT extends AbstractSpringIT {

    @Value("${local.server.port}")
    private int port = 0;

    @Test public void
    should_serve_welcome_page_if_not_authorized() throws Exception {
        ResponseEntity<String> entity = new TestRestTemplate().getForEntity(
                "http://localhost:" + this.port, String.class);

        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(entity.getBody(), containsString("<title>Instagram Collage"));
        assertThat(entity.getBody(), containsString("href=\"/login\""));
    }

    @Test
    public void
    should_serve_scc() throws Exception {
        ResponseEntity<String> entity = new TestRestTemplate().getForEntity(
                "http://localhost:" + this.port
                        + "/css/app.css", String.class);

        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(entity.getBody(), containsString("/*Custom CSS*/"));
    }

    @Test
    public void
    should_serve_js() throws Exception {
        ResponseEntity<String> entity = new TestRestTemplate().getForEntity(
                "http://localhost:" + this.port
                        + "/js/app.js", String.class);

        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(entity.getBody(), containsString("angular.module('collage'"));
    }

}
